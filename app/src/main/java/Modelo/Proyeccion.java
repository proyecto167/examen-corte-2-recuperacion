package Modelo;

import android.database.Cursor;

import com.example.examenc2recuperacion.Imc;

import java.util.ArrayList;

public interface Proyeccion {

    public Imc getImc(String id);
    public ArrayList<Imc> allImcs();
    public Imc readImc(Cursor cursor);

}

package Modelo;

import com.example.examenc2recuperacion.Imc;

public interface Persistencia {

    public void openDataBase();
    public void closeDataBase();
    public long insertImc(Imc imc);
    public long updateImc(Imc imc);
    public void deleteImcs(int id);

}

package Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ImcDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE=" TEXT";
    private static final String INTEGER_TYPE=" INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP=" ,";

    private static final String SQL_CREATE_IMC = "CREATE TABLE " +
            DefineTable.Imcs.TABLE_NAME + " ("+
            DefineTable.Imcs.COLUMN_NAME_ID + " INTEGER PRIMARY KEY, "+
            DefineTable.Imcs.COLUMN_NAME_ALTURA + REAL_TYPE + COMMA_SEP +
            DefineTable.Imcs.COLUMN_NAME_PESO + REAL_TYPE + COMMA_SEP +
            DefineTable.Imcs.COLUMN_NAME_IMC + REAL_TYPE + ")";

    private static final String SQL_DELETE_IMC = "DROP TABLE IF EXISTS " +
            DefineTable.Imcs.TABLE_NAME;

    private static final String DATABASE_NAME="sistemaimc1.db";
    private static final int DATABASE_VERSION=1;

    public ImcDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_IMC);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_IMC);
        onCreate(sqLiteDatabase);
    }
}

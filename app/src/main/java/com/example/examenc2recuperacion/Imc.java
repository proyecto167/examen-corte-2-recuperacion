package com.example.examenc2recuperacion;

import java.io.Serializable;
import java.util.ArrayList;

public class Imc implements Serializable {
    private int id;
    private float Altura;
    private float Peso;
    private float IMC;

    public Imc() {

    }

    public Imc(float Altura, float Peso, float IMC){
        this.Altura = Altura;
        this.Peso = Peso;
        this.IMC =IMC;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public float getTextAltura() { return Altura; }
    public void setTextAltura(float Altura) { this.Altura = Altura; }
    public float getTextPeso() { return Peso; }
    public void setTextPeso(float Peso) { this.Peso = Peso; }
    public float getTextIMC(){ return IMC; }
    public void setTextIMC(float IMC){
        this.IMC = IMC;
    }

    public static ArrayList<Imc> llenarImcs(){
        ArrayList<Imc> imcs=new ArrayList<>();

        imcs.add(new Imc(1.70f,60,18));
        imcs.add(new Imc(1.70f,60,18));
        imcs.add(new Imc(1.70f,60,18));


        return imcs;
    }

}


package com.example.examenc2recuperacion;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Modelo.ImcDb;

public class HistorialActivity extends AppCompatActivity {

    private ListView listView;
    private AdapterImc adapter;
    private List<Imc> listaImcs;
    private ImcDb imcDb;
    private Button btnRegresar;
    private Button btnBorrarHistorial;
    private TextView lblIMCPromedio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.historial_activity);

        imcDb = new ImcDb(this);

        // Obtener la referencia de la ListView
        listView = findViewById(R.id.lstHistorial);

        btnRegresar = findViewById(R.id.btnRegresar);
        btnBorrarHistorial = findViewById(R.id.btnBorrarHistorial);
        lblIMCPromedio = findViewById(R.id.lblIMCPromedio);

        // Crear la lista de imcs
        ArrayList<Imc> imcs = imcDb.allImcs();
        adapter = new AdapterImc(this, imcs);
        listView.setAdapter(adapter);

        calcularPromedio();

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Regresar a la pantalla anterior
                Intent intent = new Intent(HistorialActivity.this, MainActivity.class);
                startActivity(intent);
                finish(); // Cierra la actividad actual
            }
        });

        btnBorrarHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Crear una instancia de ImcDb para acceder a la base de datos
                imcDb = new ImcDb(HistorialActivity.this);

                // Llamar al método deleteAllImcs() para borrar todos los registros
                imcDb.deleteAllImcs();

                adapter.notifyDataSetChanged();
                // Mostrar un mensaje indicando que el historial ha sido borrado
                Toast.makeText(HistorialActivity.this, "Historial borrado", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void calcularPromedio() {
        float total = 0;
        listaImcs = imcDb.allImcs();

        for (Imc imc : listaImcs) {
            total += imc.getTextIMC();
        }

        if (listaImcs.size() > 0) {
            float promedio = total / listaImcs.size();
            lblIMCPromedio.setText("IMC Promedio: "+String.valueOf(promedio));
        } else {
            lblIMCPromedio.setText("IMC Promedio: 0");
        }
    }

}
package com.example.examenc2recuperacion;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.ImcDb;

public class MainActivity extends AppCompatActivity {

    private EditText txtAltura;
    private EditText txtPeso;
    private TextView lblIMC;
    private Imc nuevoImc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Enlazar vistas con las variables
        TextView lblTitulo = findViewById(R.id.lblTitulo);
        TextView lblAltura = findViewById(R.id.lblAltura);
        TextView lblPeso = findViewById(R.id.lblPeso);
        lblIMC = findViewById(R.id.lblIMC);
        txtAltura = findViewById(R.id.txtAltura);
        txtPeso = findViewById(R.id.txtPeso);
        Button btnCalcular = findViewById(R.id.btnCalcular);
        Button btnGuardar = findViewById(R.id.btnGuardar);
        Button btnVerHistorial = findViewById(R.id.btnVerHistorial);
        Button btnSalir = findViewById(R.id.btnSalir);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularIMC();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nuevoImc.getTextIMC() != 0 || nuevoImc != null) {
                    // Guardar el objeto Imc en la base de datos
                    ImcDb imcDb = new ImcDb(MainActivity.this);
                    long resultado = imcDb.insertImc(nuevoImc);

                    if (resultado != -1) {
                        Toast.makeText(MainActivity.this, "Entrada guardada en la base de datos", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Error al guardar en la base de datos", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Por favor ingresa altura, peso e IMC", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnVerHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HistorialActivity.class);
                startActivity(intent);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });
    }

    private void calcularIMC() {
        // Obtener los valores de altura y peso
        String alturaStr = txtAltura.getText().toString();
        String pesoStr = txtPeso.getText().toString();

        if (!alturaStr.isEmpty() && !pesoStr.isEmpty()) {
            // Convertir los valores a números
            float altura = Float.parseFloat(alturaStr);
            float peso = Float.parseFloat(pesoStr);

            // Calcular el índice de masa corporal (IMC)
            float imc = peso / (altura * altura);

            // Convertir el valor del IMC a String y mostrarlo en el TextView lblIMC
            lblIMC.setText("IMC:"+String.valueOf(imc)+" kg/m2");

            // Crear un objeto Imc con los valores
            nuevoImc = new Imc();
            nuevoImc.setTextAltura(altura);
            nuevoImc.setTextPeso(peso);
            nuevoImc.setTextIMC(imc);
        } else {
            Toast.makeText(this, "Por favor ingresa altura y peso", Toast.LENGTH_SHORT).show();
        }
    }
}